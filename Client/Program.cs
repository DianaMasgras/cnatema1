﻿using Grpc.Core;
using System;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 14154;

            var channel =  GrpcChannel.ForAddress("https://localhost:5001");


            Console.WriteLine("Nume: ");

            do
            {
                var key = Console.ReadLine();

                var client = new Generated.NameRequestService.NameRequestServiceClient(channel);

                client.OutputName(new Generated.Name
                {
                    Name_ = key
                });
            } while (true);

            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Apasa orice tasta pentru a iesi...");
            Console.ReadKey();
        }
    }
}
